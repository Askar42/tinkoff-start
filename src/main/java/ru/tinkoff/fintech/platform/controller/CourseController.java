package ru.tinkoff.fintech.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.platform.model.Course;
import ru.tinkoff.fintech.platform.service.CourseService;

import java.util.UUID;

@RequestMapping("/course")
@RestController()
public class CourseController {

    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping()
    public void addCourse(@RequestBody Course course) {
        courseService.save(course);
    }

    @DeleteMapping()
    public void deleteCourse(@RequestParam UUID id) {
        courseService.delete(courseService.findCourse(id));
    }

    @GetMapping()
    public Course findCourse(@RequestParam UUID id) {
        return courseService.findCourse(id);
    }

    @PutMapping
    public void editCourse(@RequestBody Course newCourse, @RequestParam UUID id) {
        Course oldCourse = courseService.findCourse(id);
        oldCourse.setTitle(newCourse.getTitle());
        oldCourse.setDescription(newCourse.getDescription());
        oldCourse.setRequiredGrade(newCourse.getRequiredGrade());
    }

    @PostMapping("/add_student")
    public void addStudent(@RequestParam UUID courseId, @RequestParam UUID studentId) {
        courseService.addStudent(courseId, studentId);
    }
}
