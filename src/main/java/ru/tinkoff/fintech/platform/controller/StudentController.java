package ru.tinkoff.fintech.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.platform.model.Student;
import ru.tinkoff.fintech.platform.service.StudentService;

import java.util.UUID;

@RequestMapping("/student")
@RestController()
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping()
    public void addStudent(@RequestBody Student student) {
        studentService.save(student);
    }

    @DeleteMapping()
    public void deleteStudent(@RequestParam UUID id) {
        studentService.delete(studentService.findStudent(id));
    }

    @GetMapping()
    public Student findStudent(@RequestParam UUID id) {
        return studentService.findStudent(id);
    }

    @PutMapping()
    public void editStudent(@RequestBody Student newStudent, @RequestParam UUID id) {
        Student olgStudent = studentService.findStudent(id);
        olgStudent.setName(newStudent.getName());
        olgStudent.setAge(newStudent.getAge());
        olgStudent.setLessonsBeginning(newStudent.getLessonsBeginning());
        olgStudent.setLessonsEnding(newStudent.getLessonsEnding());
        olgStudent.setGrade(newStudent.getGrade());
    }
}
