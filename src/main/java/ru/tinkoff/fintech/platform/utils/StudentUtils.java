package ru.tinkoff.fintech.platform.utils;

import ru.tinkoff.fintech.platform.model.Student;

public class StudentUtils {

    private StudentUtils() {}

    public static Student createStudent(String[] s) {
        return Student.of(s[0], Integer.parseInt(s[1]), Integer.parseInt(s[2]), Integer.parseInt(s[3]), 0);
    }
}
