package ru.tinkoff.fintech.platform.utils;

import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.tinkoff.fintech.platform.model.Student;

import java.io.FileReader;
import java.util.Collections;
import java.util.List;

@Component
public class Parser {

    private static final Logger log = LoggerFactory.getLogger(Parser.class);

    public List<Student> parse(FileReader fileReader) {
        try (CSVReader reader = new CSVReader(fileReader)) {
            reader.readNext();
            return reader.readAll().stream()
                    .map(StudentUtils::createStudent)
                    .toList();
        } catch (Exception e) {
            log.error("Ошибка при чтении файла: {}", e.getMessage());
            return Collections.emptyList();
        }
    }
}
