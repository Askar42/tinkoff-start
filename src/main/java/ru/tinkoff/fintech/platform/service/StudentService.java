package ru.tinkoff.fintech.platform.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.platform.dao.StudentRepository;
import ru.tinkoff.fintech.platform.model.Student;
import ru.tinkoff.fintech.platform.utils.Parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class StudentService {

    private final StudentRepository studentRepository;
    private static final Logger log = LoggerFactory.getLogger(StudentService.class);
    private static final String PATH_TO_STUDENT_CSV = "src/main/resources/students-eb533d29-9e13-4da5-9479-60b262e32ad9.csv";

    private final Parser parser;

    @Autowired
    public StudentService(StudentRepository studentRepository, Parser parser) {
        this.studentRepository = studentRepository;
        this.parser = parser;
    }

    public void save(Student student) {
        studentRepository.save(student);
    }

    public Student findStudent(UUID id) {
        return studentRepository.findById(id).orElseThrow();
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public void delete(Student student) {
        studentRepository.delete(student);
    }

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.HOURS)
    public void getBusyStudents() throws FileNotFoundException {
        int now = LocalDateTime.now().getHour();
        List<Student> busyStudents = parser.parse(new FileReader(PATH_TO_STUDENT_CSV))
                .stream().filter(s -> s.getLessonsBeginning() <= now && now < s.getLessonsEnding())
                .toList();

        log.info("Список занятых студентов: {}", busyStudents.stream().map(Student::getName).toList());
    }
}
