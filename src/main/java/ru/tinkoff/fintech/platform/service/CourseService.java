package ru.tinkoff.fintech.platform.service;

import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.platform.dao.CourseRepository;
import ru.tinkoff.fintech.platform.dao.StudentRepository;
import ru.tinkoff.fintech.platform.model.Course;
import ru.tinkoff.fintech.platform.model.Student;
import ru.tinkoff.fintech.platform.validation.Eligible;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final Validator validator;

    @Autowired
    public CourseService(CourseRepository courseRepository, StudentRepository studentRepository, Validator validator) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
        this.validator = validator;
    }

    public void save(Course course) {
        courseRepository.save(course);
    }

    public Course findCourse(UUID id) {
        return courseRepository.findById(id).orElseThrow();
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public void delete(Course course) {
        courseRepository.delete(course);
    }

    public void addStudent(UUID courseId, UUID studentId) {

        Course course = courseRepository.findById(courseId).orElseThrow();
        Student student = studentRepository.findById(studentId).orElseThrow();
        Set<ConstraintViolation<CourseAndStudent>> violations = validator.validate(new CourseAndStudent(course, student));
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }

        courseRepository.addStudent(course, student);
    }

    @Eligible
    @Value
    public static class CourseAndStudent {
        Course course;
        Student student;
    }
}

