package ru.tinkoff.fintech.platform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {

    @NotNull
    @NotEmpty
    private UUID id;

    @NotNull
    @NotEmpty
    private String title;

    @NotNull
    @NotEmpty
    private String description;

    @NotNull
    @NotEmpty
    private int requiredGrade;

    public static Course of(String title, String description, int requiredGrade) {
        return new Course(UUID.randomUUID(), title, description, requiredGrade);
    }
}
