package ru.tinkoff.fintech.platform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @NotNull
    @NotEmpty
    private UUID id;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private int age;

    @NotNull
    @NotEmpty
    private int lessonsBeginning;

    @NotNull
    @NotEmpty
    private int lessonsEnding;

    @NotNull
    @NotEmpty
    private int grade;

    public static Student of(String name, int age, int lessonsBeginning, int lessonsEnding, int grade) {
        return new Student(UUID.randomUUID(), name, age, lessonsBeginning, lessonsEnding, grade);
    }
}
