package ru.tinkoff.fintech.platform.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.fintech.platform.model.Student;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface StudentRepository {

    void save(Student student);

    List<Student> findAll();

    Optional<Student> findById(UUID id);

    void delete(Student student);
}
