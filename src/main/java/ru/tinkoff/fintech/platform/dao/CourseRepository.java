package ru.tinkoff.fintech.platform.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.tinkoff.fintech.platform.model.Course;
import ru.tinkoff.fintech.platform.model.Student;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface CourseRepository {

    void save(Course course);

    List<Course> findAll();

    Optional<Course> findById(UUID id);

    void delete(Course course);

    void addStudent(@Param("course") Course course, @Param("student") Student student);
}
