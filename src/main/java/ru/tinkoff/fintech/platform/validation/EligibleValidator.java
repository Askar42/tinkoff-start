package ru.tinkoff.fintech.platform.validation;

import ru.tinkoff.fintech.platform.service.CourseService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EligibleValidator implements ConstraintValidator<Eligible, CourseService.CourseAndStudent> {

    @Override
    public void initialize(Eligible constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(
            CourseService.CourseAndStudent courseAndStudent,
            ConstraintValidatorContext constraintValidatorContext
    ) {
        return courseAndStudent.getStudent().getGrade() >= courseAndStudent.getCourse().getRequiredGrade();
    }
}
