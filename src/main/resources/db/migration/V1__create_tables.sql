CREATE TABLE students
(
    id                UUID        PRIMARY KEY,
    name              VARCHAR(64) NOT NULL,
    age               INT         NOT NULL,
    lessons_beginning INT         NOT NULL,
    lessons_ending    INT         NOT NULL,
    grade             INT         NOT NULL
);

CREATE TABLE courses
(
    id                UUID        PRIMARY KEY,
    title             VARCHAR(64) NOT NULL,
    description       LONGTEXT    NOT NULL,
    required_grade    INT         NOT NULL
);

CREATE TABLE courses_x_students
(
    course_id         UUID        NOT NULL,
    student_id        UUID        NOT NULL,
    FOREIGN KEY (course_id) REFERENCES courses(id),
    FOREIGN KEY (student_id) REFERENCES students(id),
    UNIQUE (course_id, student_id)
);